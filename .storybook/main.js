const path = require("path");

module.exports = {
    stories: [
        "../src/**/*.stories.mdx",
        "../src/**/*.stories.@(js|jsx|ts|tsx)",
    ],
    staticDirs: ["../public"],
    addons: [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        "@storybook/addon-interactions",
        "storybook-addon-next-router",
        "storybook-dark-mode",
    ],
    core: {
        builder: "webpack5",
    },
    webpackFinal: (config) => {
        config.resolve.alias = {
            //...config.resolve?.alias,
            "@": [
                path.resolve(__dirname, "../src/"),
            ],
        };
        config.resolve.roots = [
            path.resolve(__dirname, "../public"),
            "node_modules",
        ];

        return config;
    },
    env: (config) => ({
        ...config,
        NEXT_PUBLIC_STORYBOK: "TRUE",
        NEXT_PUBLIC_MOCKDATA: "TRUE",
    }),
};
