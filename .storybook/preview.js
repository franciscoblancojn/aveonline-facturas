
import * as NextImage from "next/image";
import 'fenextjs-component/styles/index.css'
import "../src/styles/style.css"

const OriginalNextImage = NextImage.default;

Object.defineProperty(NextImage, "default", {
  configurable: true,
  value: (props) => <OriginalNextImage {...props} unoptimized />,
});