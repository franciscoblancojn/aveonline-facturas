export const url = {
    home: '/',
    checkout: '/checkout',
    detalle: '/detalle-factura',
};
export default url;
