import Whatsapp from '@/svg/Whatsapp';
import Link from 'next/link';

export interface HeadersProps {
    home?: string;
    titleHeader?: string;
    whatsapp?: string;
    whatsappMovil?: string;
}

export const Header = ({
    home = '/',
    whatsapp = 'https://web.whatsapp.com/send?phone=573233162889&text=Hola%2C%20estaba%20en%20https%3A%2F%2Fcobrocartera.aveonline.co%20me%20gustar%C3%ADa%20recibir%20m%C3%A1s%20informaci%C3%B3n.',
    whatsappMovil = 'https://api.whatsapp.com/send?phone=573233162889&text=Hola%2C%20estaba%20en%20https%3A%2F%2Fcobrocartera.aveonline.co%20me%20gustar%C3%ADa%20recibir%20m%C3%A1s%20informaci%C3%B3n.',
}: HeadersProps) => {
    return (
        <>
            <div className="ConentHeader">
                <div className="ContentLogo">
                    <Link href={home}>
                        <img
                            className="Logo"
                            src="./images/AveonlineLogo.png"
                            alt=""
                        />
                    </Link>
                </div>

                <div className="ContentWhatsapp">
                    <Link href={whatsapp}>
                        <div className="Whatsapp">
                            <Whatsapp size={22} />
                            <span className="Por">
                                ¿Por qué estoy recibiendo esto?
                            </span>
                        </div>
                    </Link>
                </div>
                <div className="ContentWhatsappMovil">
                    <Link href={whatsappMovil}>
                        <div className="WhatsappMovil">
                            <Whatsapp size={18} />
                            <span className="PorMovil">
                                ¿Por que estoy recibiendo esto?
                            </span>
                        </div>
                    </Link>
                </div>
            </div>
        </>
    );
};
