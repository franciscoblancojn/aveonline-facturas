import React, { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { Header, HeadersProps } from "./index";

export default {
    title: "Component/Header",
    component: Header,
} as Meta;

const Profile: Story<PropsWithChildren<HeadersProps>> = (args) => (
    <Header {...args}>Test Children</Header>
);

export const Index = Profile.bind({});
Index.args = {};
