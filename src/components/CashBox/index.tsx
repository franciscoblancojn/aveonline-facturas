import { Button } from 'fenextjs-component/cjs/Button';
import React, { useRef } from 'react';
import sha1 from 'sha1';
import { RenderLoader } from '../Loader';
import LoaderPrice from '../LoaderPrice';

export interface CashBoxProps {
    money: number | undefined;
    loader: boolean;
    nit?: number;
    prefijoFactura: string;
    numeroFactura: number;
    title?: string;
    textButton?: string;
}

export function createHashBill(
    nit: number,
    prefijoFactura: string,
    numeroFactura: number
) {
    if (!Number.isInteger(nit)) {
        throw new Error('The billing document expected a number');
    }
    if (!Number.isInteger(numeroFactura)) {
        throw new Error('The invoice id a number was expected');
    }
    if (typeof prefijoFactura !== 'string') {
        throw new Error('The invoice prefijoFactura is not a text string');
    }

    const param = `${nit}${prefijoFactura}${numeroFactura}`.toUpperCase();

    return sha1(param);
}

export const CashBox = ({
    money = '0',
    loader = false,
    nit = 1,
    prefijoFactura = 'ST 6432',
    numeroFactura = 11111,
    title = 'Deuda total',
    textButton = 'Pagar',
}) => {
    const formRef = useRef<HTMLFormElement>(null);

    const handleClick = () => {
        formRef.current?.submit();
    };

    const nitNumber = Number(nit);
    const numeroFacturaNumber = Number(numeroFactura);

    const hashBill = createHashBill(
        nitNumber,
        prefijoFactura,
        numeroFacturaNumber
    );

    return (
        <div className="ConentCashBox">
            <div className="Content">
                <h2 className="Title">{title}</h2>
                <RenderLoader loader={loader} componentLoader={<LoaderPrice />}>
                    <h1 className="Price">{money}</h1>
                </RenderLoader>
            </div>
            <div>
                <form
                    ref={formRef}
                    action="https://app.aveonline.co/wompi/index.php"
                    className="hidden"
                >
                    <input type="hidden" id="w" name="w" value={hashBill} />
                    <input type="hidden" name="nit" value={nit} />
                    <input
                        type="hidden"
                        name="prefijoFactura"
                        value={prefijoFactura}
                    />
                    <input
                        type="hidden"
                        name="numeroFactura"
                        value={numeroFactura}
                    />
                </form>

                <Button onClick={handleClick}>{textButton}</Button>
            </div>
        </div>
    );
};

export default CashBox;
