import React from "react";
import { Story, Meta } from "@storybook/react";

import { CashBox ,CashBoxProps } from "./index";

export default {
    title: "Component/CashBox",
    component: CashBox,
} as Meta;

const Profile: Story<CashBoxProps> = () => (
    <CashBox ></CashBox>
);

export const Index = Profile.bind({});
Index.args = {};
