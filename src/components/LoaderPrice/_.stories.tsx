import React, { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { LoaderPrice, LoaderPriceProps } from "./index";

export default {
    title: "Component/LoaderPrice",
    component: LoaderPrice,
} as Meta;

const Profile: Story<PropsWithChildren<LoaderPriceProps>> = (args) => (
    <LoaderPrice {...args}>Test Children</LoaderPrice>
);

export const Index = Profile.bind({});
Index.args = {};
