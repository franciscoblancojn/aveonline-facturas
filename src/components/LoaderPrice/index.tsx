import React from 'react';
import ContentLoader from 'react-content-loader';

export interface LoaderPriceProps {}

export const LoaderPrice = (props: any) => (
    <ContentLoader
        speed={2}
        viewBox="0 0 500 160"
        backgroundColor="#828282"
        foregroundColor="#ffffff"
        {...props}
    >
        <rect x="12" y="71" rx="3" ry="3" width="479" height="19" />
        <rect x="12" y="26" rx="3" ry="3" width="296" height="21" />
        <rect x="546" y="199" rx="0" ry="0" width="158" height="96" />
    </ContentLoader>
);

export default LoaderPrice;
