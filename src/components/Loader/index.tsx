import { PropsWithChildren } from 'react';

export interface RenderLoaderProps extends PropsWithChildren {
    loader: boolean;
    componentLoader?: any;
}

export const RenderLoader = ({
    loader,
    componentLoader = 'Cargando',
    children,
}: RenderLoaderProps) => {
    return <>{loader ? componentLoader : children}</>;
};
