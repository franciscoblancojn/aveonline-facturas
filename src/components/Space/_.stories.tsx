

import React, { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { Space, SpaceProps } from "./index";

export default {
    title: "Component/Space",
    component: Space,
} as Meta;

const Profile: Story<PropsWithChildren<SpaceProps>> = (args) => (
    <Space {...args}>Test Children</Space>
);

export const Index = Profile.bind({});
Index.args = {};
