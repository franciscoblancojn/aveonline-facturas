# Space

## Import

```js
import { Space } from '@/components/Space';
```

## Props

```tsx
export interface SpaceProps {
    size: number;
}
```

## Use

```js
<Space size={10} />
```
