import { Backs } from '@/components/Back';

import { url } from '@/config/routes';
import React, { useState } from 'react';
import Space from '../Space';
import { FacturaProps } from '@/interfaces/Factura';
import { useRequest } from 'fenextjs-hook/cjs/useRequest';

import { getFacturas, getFacturasProps } from '@/api/Cartera';
import { RequestResultTypeProps } from 'fenextjs-interface/cjs/Request';
import { parseMoney } from 'fenextjs-functions/cjs/parse/Money';
import { Button } from 'fenextjs-component/cjs/Button';
import log from '@/functions/log';

export interface CheckoutProps {}

const query: getFacturasProps = {
    fechaFactura: new Date(0),
    fechaVencimiento: new Date('2023-03-01'),
    abonosRealizados: null,
    factura: '',
    cliente: '',
    id: '',
    saldoPagar: null,
    valorFactura: null,
    nit: null,
    numeroFactura: null,
    prefijoFactura: '',
    observacion: '',
};

export const Checkout: React.FC<any> = ({}) => {
    const [hast] = useState('');
    const [customAmount] = useState('');
    const [isCustomAmountEnabled, setIsCustomAmountEnabled] = useState(false);

    function handleOptionChange(e: React.ChangeEvent<HTMLInputElement>) {
        setIsCustomAmountEnabled(e.target.value === 'otro-monto');
    }

    const { result, loader } = useRequest<
        getFacturasProps,
        FacturaProps[],
        any,
        RequestResultTypeProps
    >({
        query,
        request: getFacturas,
        autoRequest: true,
    });
    log('', {
        result,
        loader,
    });

    /*
    const TolPag =
        result?.result?.reduce((a, c) => a + (c.saldoPagar ?? 0), 0) ?? 0;
    const loadHast = async () => {
        const newHast = await createHashBill(
            query.nit ?? 0, // si query.nit es nulo, usa 0 en su lugar
            query.prefijoFactura,
            query.numeroFactura ?? 0 // si query.numeroFactura es nulo, usa 0 en su lugar
        );
        sethast(newHast);
    };
    useEffect(() => {
        loadHast();
    }, []);

    */

    return (
        <div className="ConentCheckout">
            <Backs href={url.home} />
            <Space size={20} />
            <span className="Text-select">
                Selecciona valor que deseas pagar
            </span>
            <Space size={10} />
            <form action={`https://app.aveonline.co/wompi/index.php`}>
                <input type="hidden" id="w" name="w" value={hast} />
                <div id="form-hidden" className="hidden" />
                <div className="deuda-total">
                    <input
                        type="radio"
                        id="deuda-total"
                        name="selectedOption"
                        value="deuda-total"
                        onChange={handleOptionChange}
                        required
                    />
                    <label htmlFor="deuda-total" className="deuda-total-text">
                        Deuda total
                    </label>
                    <span className="Total-pagar"></span>
                </div>
                <Space size={10} />
                <div className="otro-monto">
                    <input
                        type="radio"
                        id="otro-monto-checkbox"
                        name="selectedOption"
                        value="otro-monto"
                        onChange={handleOptionChange}
                        required
                    />
                    <div className="content-otro-valor">
                        <label
                            htmlFor="otro-monto-checkbox"
                            className="otro-valor-text"
                        >
                            Otro valor
                        </label>
                        <Space size={5} />
                        <input
                            className="Input-otro-valor"
                            type="number"
                            placeholder="$ Digita el valor"
                            id="monto-personalizado"
                            value={customAmount}
                            disabled={!isCustomAmountEnabled}
                            required
                        />
                        <span
                            className={
                                parseFloat(customAmount) > 0
                                    ? 'post'
                                    : 'post transparent'
                            }
                        >
                            {parseFloat(customAmount) > 0 &&
                                parseMoney(customAmount)}
                        </span>
                    </div>
                </div>
                <Space size={20} />
                <Button>Pagar</Button>
            </form>
        </div>
    );
};

export default Checkout;
