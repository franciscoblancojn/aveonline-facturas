import React, { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { Checkout, CheckoutProps } from "./index";

export default {
    title: "Component/Checkout",
    component: Checkout,
} as Meta;

const Profile: Story<PropsWithChildren<CheckoutProps>> = (args) => (
    <Checkout {...args}>Test Children</Checkout>
);

export const Index = Profile.bind({});
Index.args = {};
