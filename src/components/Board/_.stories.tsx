import React, { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { Board, BoardProps } from "./index";

export default {
    title: "Component/Board",
    component: Board,
} as Meta;

const Profile: Story<PropsWithChildren<BoardProps>> = (args) => (
    <Board {...args}>Test Children</Board>
);

export const Index = Profile.bind({});
Index.args = {};
