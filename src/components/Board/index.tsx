import React, { useState } from 'react';

import { Table, TableProps } from 'fenextjs-component/cjs/Table';
import { FacturaProps, FacturasTableHeader } from '@/interfaces/Factura';
import { RenderLoader } from '@/components/Loader';
import DataTable from '../LoaderTable';
import Paginator from '@/components/Paginator';

export interface BoardProps extends Pick<TableProps<FacturaProps>, 'items'> {
    loader: boolean;
}

export const Board = ({ items, loader = false }: BoardProps) => {
    const [currentPage, setCurrentPage] = useState(1); // Añade el estado para el número de página actual
    const itemsPerPage = 10; // Define el número de elementos por página
    const totalPages = Math.ceil(items.length / itemsPerPage); // Calcula el número total de páginas

    // Filtra los elementos para mostrar solo los correspondientes a la página actual
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    const filteredItems = items.slice(startIndex, endIndex);

    const handlePageChange = (page: number) => {
        setCurrentPage(page);
    };

    return (
        <RenderLoader loader={loader} componentLoader={<DataTable />}>
            <div className="ConentBoard">
                <Table<FacturaProps>
                    classNameTh="classNameTh"
                    classNameTHead="classNameTHead"
                    classNameTable="classNameTable"
                    classNameTBody="classNameTBody"
                    classNameTr="classNameTr"
                    classNameTd="classNameTd"
                    classNameContentTable="classNameContentTable"
                    header={FacturasTableHeader}
                    items={filteredItems}
                />
                <Paginator
                    currentPage={currentPage}
                    totalPages={totalPages}
                    onPageChange={handlePageChange}
                />
            </div>
        </RenderLoader>
    );
};

export default Board;
