import React, { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { DataTable, DataTableProps } from "./index";

export default {
    title: "Component/DataTable",
    component: DataTable,
} as Meta;

const Profile: Story<PropsWithChildren<DataTableProps>> = (args) => (
    <DataTable {...args}>Test Children</DataTable>
);

export const Index = Profile.bind({});
Index.args = {};
