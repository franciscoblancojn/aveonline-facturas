import { Story, Meta } from "@storybook/react";

import { PaginatorProps, Paginator } from "./index";

export default {
    title: "Paginator/Paginator",
    component: Paginator,
} as Meta;

const PaginatorIndex: Story<PaginatorProps> = (args) => (
    <Paginator {...args}>Test Children</Paginator>
);

export const Index = PaginatorIndex.bind({});
Index.args = {
    
};
