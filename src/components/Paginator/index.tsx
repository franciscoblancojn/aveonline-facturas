import React, { useState } from 'react';
import ArrowLeft from '@/svg/ArrowLeft';
import ArrowRight from '@/svg/ArrowRight';

export interface PaginatorProps {
    currentPage: number;
    totalPages: number;
    onPageChange: (page: number) => void;
}

export const Paginator = ({
    currentPage,
    totalPages,
    onPageChange,
}: PaginatorProps) => {
    const [showFirstPage, setShowFirstPage] = useState(true);

    const handlePageChange = (page: number) => {
        if (page < 1 || page > totalPages) {
            return;
        }
        if (page >= 5) {
            setShowFirstPage(false);
        } else if (page === 1) {
            setShowFirstPage(true);
        }
        onPageChange(page);
    };

    // Ejemplo de uso de showFirstPage
    if (showFirstPage) {
        // TODO: Agregar código aquí
    }

    const renderArrow = (direction: 'left' | 'right') => {
        const isDisabled =
            (direction === 'left' && currentPage === 1) ||
            (direction === 'right' && currentPage === totalPages);
        const className = `Paginator__arrow Paginator__arrow--${direction}${
            isDisabled ? ' Paginator__arrow--disabled' : ''
        }`;
        const onClick = () =>
            handlePageChange(currentPage + (direction === 'left' ? -1 : 1));
        const label = direction === 'left' ? 'Anterior' : 'Siguiente';
        const arrowIcon =
            direction === 'left' ? (
                <ArrowLeft size={8} />
            ) : (
                <ArrowRight size={8} />
            );
        return (
            <div
                className={className}
                onClick={onClick}
                aria-label={label}
                role="button"
                tabIndex={0}
            >
                {arrowIcon}
            </div>
        );
    };

    const renderPageNumbers = () => {
        const pageNumbers = [];

        for (let i = 1; i <= totalPages; i++) {
            pageNumbers.push(i);
        }

        let startPage = 1;
        if (currentPage > 3 && currentPage < totalPages - 1) {
            startPage = currentPage - 2;
        } else if (currentPage >= totalPages - 1) {
            startPage = totalPages - 4;
        }

        const visiblePageNumbers = pageNumbers.slice(
            startPage - 1,
            startPage + 4
        );

        return (
            <div className="Paginator__numbers">
                {visiblePageNumbers.map((number) => (
                    <div
                        key={number}
                        className={`Paginator__number${
                            currentPage === number
                                ? ' Paginator__number--active'
                                : ''
                        }`}
                        onClick={() => handlePageChange(number)}
                        role="button"
                        tabIndex={0}
                    >
                        {number}
                    </div>
                ))}
            </div>
        );
    };

    return (
        <div className="Paginator">
            {renderArrow('left')}
            <div className="Paginator__pages">{renderPageNumbers()}</div>
            {renderArrow('right')}
        </div>
    );
};

export default Paginator;
