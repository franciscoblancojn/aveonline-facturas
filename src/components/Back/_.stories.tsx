import React, { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { Backs, BacksProps } from "./index";

export default {
    title: "Component/Backs",
    component: Backs,
} as Meta;

const Profile: Story<PropsWithChildren<BacksProps>> = (args) => (
    <Backs {...args}>Test Children</Backs>
);

export const Index = Profile.bind({});
Index.args = {};
