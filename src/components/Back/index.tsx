import { Back } from '@/svg/Add';
import Link from 'next/link';

export interface BacksProps {
    href?: string;
    titleBack?: string;
}

export const Backs = ({ href = '/', titleBack = 'Atrás' }: BacksProps) => {
    return (
        <>
            <div className="ContentLink">
                <Link href={href}>
                    <div className="ConentBacks">
                        <Back size={12} />
                        <h1 className="Title">{titleBack}</h1>
                    </div>
                </Link>
            </div>
        </>
    );
};
