import React, { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { FormPost, FormPostProps } from "./index";

export default {
    title: "Component/FormPost",
    component: FormPost,
} as Meta;

const Profile: Story<PropsWithChildren<FormPostProps>> = (args) => (
    <FormPost {...args}>Test Children</FormPost>
);

export const Index = Profile.bind({});
Index.args = {};
