import { useState } from 'react';

export interface FormPostProps {
    titleFormPost?: string;
}

export const FormPost = ({}: FormPostProps) => {
    const [recipient, setRecipient] = useState('');
    const [message, setMessage] = useState('');
    const [status, setStatus] = useState('');

    const handleSubmit = async (event: any) => {
        event.preventDefault();

        const response = await fetch('/api/send-sms', {
            method: 'POST',
            body: JSON.stringify({ recipient, message }),
            headers: {
                'Content-Type': 'application/json',
            },
        });

        const data = await response.json();
        setStatus(data.message);
    };
    return (
        <>
            <div className="ConentFormPost">
                <div>
                    <form onSubmit={handleSubmit}>
                        <label htmlFor="recipient">Recipient:</label>
                        <input
                            id="recipient"
                            type="tel"
                            value={recipient}
                            onChange={(event) =>
                                setRecipient(event.target.value)
                            }
                            required
                        />

                        <label htmlFor="message">Message:</label>
                        <textarea
                            id="message"
                            value={message}
                            onChange={(event) => setMessage(event.target.value)}
                            required
                        />

                        <button type="submit">Send SMS</button>
                    </form>

                    {status && <p>{status}</p>}
                </div>
            </div>
        </>
    );
};
