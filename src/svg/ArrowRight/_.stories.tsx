import { Story, Meta } from "@storybook/react";

import { ArrowRight } from "./index";

export default {
    title: "Svg/ArrowRight",
    component: ArrowRight,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <ArrowRight {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
