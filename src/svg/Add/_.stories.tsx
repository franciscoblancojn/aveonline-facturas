import { Story, Meta } from "@storybook/react";

import { Back } from "./index";

export default {
    title: "Svg/Back",
    component: Back,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Back {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
