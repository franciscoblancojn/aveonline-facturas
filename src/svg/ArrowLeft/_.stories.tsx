import { Story, Meta } from "@storybook/react";

import { ArrowLeft } from "./index";

export default {
    title: "Svg/ArrowLeft",
    component: ArrowLeft,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <ArrowLeft {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
