import { TableHeader } from 'fenextjs-component/cjs/Table';
import { parseDateYYYYMMDD } from 'fenextjs-functions/cjs/parse/Date';
import { parseMoney } from 'fenextjs-functions/cjs/parse/Money';

export interface FacturaProps {
    id: string;
    cliente: string;
    nit: number;
    factura: string;
    fechaFactura: Date;
    fechaVencimiento: Date;
    observacion: string;
    valorFactura: number;
    abonosRealizados: number;
    saldoPagar: number;
    prefijoFactura: string;
    numeroFactura: number;
    correocliente: string;
}

export const FacturasTableHeader: TableHeader<FacturaProps> = [
    {
        id: 'id',
        th: 'Factura',
    },
    {
        id: 'numeroFactura',
        th: 'Número de factura',
    },
    {
        id: 'fechaFactura',
        th: 'Fecha facturación',
        parse: (data) => {
            return parseDateYYYYMMDD(data.fechaFactura);
        },
    },
    {
        id: 'fechaVencimiento',
        th: 'Fecha vencimiento',
        parse: (data) => {
            return parseDateYYYYMMDD(data.fechaVencimiento);
        },
    },
    {
        id: 'correocliente',
        th: 'Correo Cliente',
    },
    {
        id: 'valorFactura',
        th: 'Valor factura',
        parse: (data) => {
            return parseMoney(data.valorFactura);
        },
    },
    {
        id: 'abonosRealizados',
        th: 'Abonos realizados',
        parse: (data) => {
            return parseMoney(data.abonosRealizados);
        },
    },
    {
        id: 'saldoPagar',
        th: 'Saldo a pagar',
        parse: (data) => {
            return parseMoney(data.saldoPagar);
        },
    },
];
