import { Header } from '@/components';
import { Board } from '@/components/Board';
import { FacturaProps } from '@/interfaces/Factura';
import { useRequest } from 'fenextjs-hook/cjs/useRequest';

import { getFacturas, getFacturasProps } from '@/api/Cartera';
import { RequestResultTypeProps } from 'fenextjs-interface/cjs/Request';
import Space from '@/components/Space';
import { useRouter } from 'next/router';
import { useMemo } from 'react';
import CashBox from '@/components/CashBox';
import { parseMoney } from 'fenextjs-functions/cjs/parse/Money';

export default function Home({}) {
    const router = useRouter();

    const query = useMemo(() => {
        const { factura } = router.query;
        const facturaConGuion = `${factura}`;
        const facturaSinGuion = facturaConGuion.replace('-', ' ');
        const r: getFacturasProps = {
            fechaFactura: new Date(0),
            fechaVencimiento: new Date('2023-04-08'),
            abonosRealizados: null,
            factura: facturaSinGuion,
            cliente: '',
            id: '',
            saldoPagar: null,
            valorFactura: null,
            nit: null,
            numeroFactura: null,
            prefijoFactura: '',
            observacion: '',
        };
        return r;
    }, [router]);

    const { result, loader } = useRequest<
        getFacturasProps,
        FacturaProps[],
        any,
        RequestResultTypeProps
    >({
        query,
        request: getFacturas,
        autoRequest: true,
    });

    const datos = useMemo(() => {
        const DATA = result?.result?.[0];
        if (!DATA) {
            return null;
        }
        return {
            nit: DATA.nit,
            prefijoFactura: DATA.prefijoFactura,
            numeroFactura: DATA.numeroFactura,
            saldoPagar: DATA.saldoPagar,
        };
    }, [result]);

    return (
        <>
            <div className="container">
                <Header />
                <Space size={20} />

                <h1 className="title">
                    Tienes facturas vencidas de hasta más de 90 días.
                </h1>
                <p className="text">
                    Sabemos que has tenido momentos difíciles y queremos
                    ayudarte, por eso antes de reportarte <br />
                    en centrales de riesgo y para que sigas usando el servicio,
                    te invitamos a ponerte al día con tu saldo pendiente.
                </p>

                <Space size={20} />

                {datos && (
                    <CashBox
                        money={parseMoney(datos.saldoPagar ?? 0)}
                        loader={loader}
                        nit={datos.nit}
                        prefijoFactura={datos.prefijoFactura}
                        numeroFactura={datos.numeroFactura}
                        title="Deuda total"
                        textButton="Pagar"
                    />
                )}

                <Space size={20} />
                <Board items={result?.result ?? []} loader={loader} />
            </div>
        </>
    );
}
