import { Header, Space } from '@/components';
import Checkout from '@/components/Checkout';

export default function Home({}) {
    return (
        <>
            <div className="container">
                <Header />
                <Space size={20} />
                <div className="content-inf-checkout">
                    <h2 className="title-facturas">Paga tus facturas</h2>
                    <p className="text-facturas">
                        Puedes hacer el pago completo o parcial de tu saldo en
                        línea, con PSE u otro medios de pago como tarjeta de
                        crédito.
                    </p>
                </div>
                <Space size={10} />
                <Checkout />
            </div>
        </>
    );
}
