import axios from 'axios';
import log from '@/functions/log';

export interface requestProps {
    method?: 'get' | 'post' | 'put' | 'delete';
    url: string;
    data?: {
        [key: string]: any;
    };
    headers?: {
        [key: string]: any;
    };
    params?: {
        [key: string]: any;
    };
}

export type requestResultStatus = 'ok' | 'error';

export interface requestResult {
    type: requestResultStatus;
    result?: any;
    error?: any;
}

export type requestFunction = (config: requestProps) => Promise<requestResult>;

export const request: requestFunction = async (config: requestProps) => {
    try {
        const response = await axios({
            method: config.method || 'get',
            url: config.url,
            data: config.data || {},
            headers: config.headers || {},
            params: config.params || {},
        });

        log('RESPOND REQUEST', response, 'green');

        return {
            type: 'ok',
            result: response.data,
        };
    } catch (error: any) {
        log('ERROR REQUEST', error, 'red');

        return {
            type: 'error',
            error,
        };
    }
};
