import { FacturaProps } from '@/interfaces/Factura';
import { request } from './request';
import {
    RequestResultDataProps,
    RequestResultTypeProps,
    RequestProps,
} from 'fenextjs-interface/cjs/Request';

export interface getFacturasProps {
    fechaFactura: Date;
    fechaVencimiento: Date;
    abonosRealizados: number | null;
    factura?: string;
    cliente: string;
    id: string;
    saldoPagar: number | null;
    valorFactura: number | null;
    nit?: string | null;
    numeroFactura: string | null;
    prefijoFactura: string;
    observacion: string;
}
export interface requestResult {
    type: RequestResultTypeProps;
    message?: string;
    error?: any;
    result?: {
        facturas: FacturaProps[];
    };
}

export const getFacturas: RequestProps<
    getFacturasProps,
    FacturaProps[],
    any,
    RequestResultTypeProps
> = async ({
    fechaFactura,
    fechaVencimiento,
    factura,
    nit,
}: getFacturasProps) => {
    const requestData = {
        tipo: 'cargarCartera',
        fechaFactura: fechaFactura.toISOString().substring(0, 10),
        fechaVencimiento: fechaVencimiento.toISOString().substring(0, 10),
        prefijo: '',
        edad: '',
        factura,
        nit,
    };

    const result = await request({
        method: 'post',
        url: 'https://app.aveonline.co/api/comunes/v1.0/administrativo/cartera.php',
        data: requestData,
    });
    if (result.type == 'ok') {
        const r = result.result;
        const respond: RequestResultDataProps<FacturaProps[]> = {
            type: RequestResultTypeProps.OK,
            message: 'Facturas Cargadas',
            result:
                r?.facturas?.map?.((factura: any) => {
                    const f: FacturaProps = {
                        id: factura.factura,
                        numeroFactura: factura.numeroFactura,
                        prefijoFactura: factura.prefijoFactura,
                        nit: factura.nit,
                        fechaVencimiento: new Date(factura.fechaVencimineto),
                        observacion: factura.observacion,
                        saldoPagar: factura.saldo,
                        valorFactura: factura.totalFactura,
                        fechaFactura: new Date(factura.fechaFactura),
                        abonosRealizados: factura.abonos,
                        cliente: factura.cliente,
                        factura: factura.factura,
                        correocliente: factura.correocliente,
                    };
                    return f;
                }) ?? [],
        };

        return respond;
    }

    const respond: RequestResultDataProps<FacturaProps[]> = {
        type: RequestResultTypeProps.ERROR,
        message: `${result.error}`,
        error: result.error,
    };

    return respond;
};
