# aveonline-facturas

## Descripción
Este proyecto es una aplicación de facturación desarrollada utilizando Next.js. Proporciona funcionalidades para generar facturas, interactuar con la base de datos utilizando Prisma ORM y realizar diversas tareas relacionadas con la gestión de facturas.

## Instalación
1. Clona este repositorio en tu máquina local.
2. Ejecuta el siguiente comando para instalar las dependencias:

npm install

## Uso
1. Para iniciar la aplicación en modo de desarrollo con datos de prueba, ejecuta el siguiente comando:

npm run mock

Esto iniciará el servidor utilizando los datos de prueba y permitirá realizar pruebas y desarrollo local.

2. Para compilar el código y construir la aplicación, ejecuta el siguiente comando:

npm run build

3. Para exportar la aplicación estática lista para implementar, ejecuta el siguiente comando:

npm run export

Esto generará una carpeta "out" con los archivos estáticos de la aplicación listos para ser implementados.

4. Para iniciar la aplicación en modo de producción, ejecuta el siguiente comando:

npm start


## Scripts adicionales
- `lint`: Ejecuta ESLint para realizar análisis estático del código.
- `lint:fix`: Ejecuta ESLint y aplica correcciones automáticas.
- `prettier`: Verifica el formato del código utilizando Prettier.
- `prettier:fix`: Aplica el formato correcto al código utilizando Prettier.
- `format`: Ejecuta `prettier:fix` y `lint:fix` para formatear y corregir el código.
- `storybook`: Inicia el entorno de Storybook para el desarrollo de componentes.
- `storybook:nocache`: Inicia el entorno de Storybook sin utilizar la caché del gestor de Storybook.
- `sass`: Compila el archivo SCSS `style.scss` a `style.css`.

## Contribución
Si deseas contribuir a este proyecto, puedes seguir los siguientes pasos:

1. Crea una bifurcación (fork) de este repositorio.
2. Clona tu bifurcación en tu máquina local.
3. Crea una nueva rama para realizar tus modificaciones.
4. Realiza los cambios necesarios y realiza confirmaciones (commits) con mensajes descriptivos.
5. Empuja (push) tus cambios a tu bifurcación en GitHub.
6. Crea una solicitud de extracción (pull request) en este repositorio.

## Licencia
Este proyecto está bajo la Licencia ISC. Consulta el archivo [LICENSE](LICENSE) para obtener más información.






